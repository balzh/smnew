import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TextFileReading {
 
    public static void main(String[] args) {

      	String firstParam = args[0];

      	String secondParam = args[1] ;

  	try {
    	FileWriter writer = new FileWriter("out.txt", true);
    	writer.write(firstParam);
    	writer.write("\r\n");
    	writer.write(secondParam);
    	writer.close();
  	} catch (IOException e) {
   		e.printStackTrace();
  	}

        try {
            FileReader reader = new FileReader("out.txt");
            BufferedReader bufferedReader = new BufferedReader(reader);
 
            String line;
 
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
            reader.close();
 
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
}
